def code_morze(value: str):
    '''
    Write functions code_morse for Morse coding.

    :param value: Text to encode

    :return: Encoded text
    '''
    MORZE_CODE = {'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.',
                  'G': '--.', 'H': '....', 'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..',
                  'M': '--', 'N': '-.', 'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.',
                  'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-',
                  'Y': '-.--', 'Z': '--..',
                  '1': '.----', '2': '..---', '3': '...--',
                  '4': '....-', '5': '.....', '6': '-....',
                  '7': '--...', '8': '---..', '9': '----.', '0': '-----',
                  ', ': '--..--', '.': '.-.-.-', '?': '..--..',
                  '/': '-..-.', '-': '-....-', '(': '-.--.', ')': '-.--.-',
                  }
    result = " ".join([MORZE_CODE[letter] for letter in value.upper() if not letter.isspace()])
    result = result.replace('-', '−')
    result = result.replace('.', '·')
    return result


if __name__ == '__main__':
    """
    Example of input: "Data Science - 2023" 
    Example of output: "−·· ·− − ·− ··· −·−· ·· · −· −·−· · −····− ··−−− −−−−− ··−−− ···−−",
    """
    test_input = "Data Science - 2023"
    test_output = "−·· ·− − ·− ··· −·−· ·· · −· −·−· · −····− ··−−− −−−−− ··−−− ···−−"
    print(test_output == code_morze(test_input))
    print(test_output)
    print(code_morze(test_input))
